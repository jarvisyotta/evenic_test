<?php

class Main extends Eloquent{
	public function getAllDataTitik(){
		$result = DB::table('data_kost')->select('id_kost','nama_kost','jenis_kost', 'lat', 'lng')->get();
		return $result;
	}

	public function getInfoTitik($id_kost){
		$data = array();

		$result = DB::table('data_kost')->where('data_kost.id_kost', '=', $id_kost)->get();
		$data['kost'] = $result;

		$result = DB::table('data_kamar_kost')->where('data_kamar_kost.id_kost', '=', $id_kost)->get();
		$data['kamar_kost'] = $result;

		return $data;
	}
}