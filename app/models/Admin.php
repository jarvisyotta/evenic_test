<?php

class Admin extends Eloquent{
	public function checkUser($username, $password){
		$valid = DB::table('user')
			->where('username', '=', $username)
			->where('password', '=', $password)
			->count();
		return $valid;
	}

	public function insertSession($username, $session){
		DB::table('user_session')->insert(array('session'=>$session, 'username'=>$username));
	}

	public function getUsername($session){
		$session = DB::table('user_session')->select('username')->first();
		return $session->username;
	}

	public function logoutUser($session){
		$username = $this->getUsername($session);
		echo $username;
		Session::forget('login_key');
		DB::table('user_session')->where('session','=',$session)->delete();
	}
}