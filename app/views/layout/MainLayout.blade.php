<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Kost-Informasi</title>
	@section('assets')
	<link rel="stylesheet" href="{{URL::to('assets/css/kost_style.css')}}">
	<script src="{{URL::to('assets/js/jquery.js')}}"></script>
	<script src="{{URL::to('assets/js/jquery.cycle2.min.js')}}"></script>
	@show
</head>
<body>

@section('header')
@show

@section('content')
@show

@section('footer')
@show

</body>
</html>