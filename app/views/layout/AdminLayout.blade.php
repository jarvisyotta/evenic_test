<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Admin #KostInformasi</title>
	<link rel="stylesheet" href="{{URL::to('assets/css/adm_style.css')}}"> 
	@section('assets')
	<script src="{{URL::to('assets/js/jquery.js')}}"></script>
	@show
</head>
<body>
@section('header')
@show

<div class="clear_both"></div>

@yield('content')

@section('footer')
@show
</body>
</html>