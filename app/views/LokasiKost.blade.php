@extends('layout.MainLayout')

@section('assets')
@parent
<link rel="stylesheet" href="{{URL::to('assets/css/overlay_style.css')}}">
<link rel="stylesheet" href="{{URL::to('assets/css/font-awesome.min.css')}}">
<script src="{{URL::to('assets/js/jquery.js')}}"></script>
<script src="{{URL::to('assets/js/overlay.js')}}"></script>
<script>
	var base_url = "http://localhost/kost_kost/";
	var point_data = <?php echo $dataTitik;?>;
	var jData = <?php echo $jData;?>;
</script>
<!-- <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?v=3.exp&libraries=visualization"></script> -->
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=visualization"></script>
<script src="{{URL::to('assets/js/gmap.js')}}"></script>
@stop

@section('header')
<header>
	<div id="content">
		<div id="top">		
		<img src="{{URL::to('/assets/img/home_new.png');}}" width="120px" class="float_left">
		<div class="webTitle float_left">
		Kost Informasi
		<div class="small_webTitle align_right">Butuh kost? Kami punya informasinya...</div>
		</div>

		<div class="clear_both margin_btm"></div>
		</div>
	</div>
</header>
@stop

@section('content')
<!-- <button id="clickMe">Hello</button> -->
<!-- <div class="showMe">Hello</div> -->
<div id="mapKost">
</div>
@stop

@section('footer')
@include('layout.Footer')
<script>
$('#clickMe').click(function(){
	console.log("Helloooooo");
});
</script>
@stop
