@extends('layout.AdminLayout')

@section('assets')
@parent
<link rel="stylesheet" href="{{URL::to('assets/css/login_style.css')}}">
@stop

@section('header')
@include('layout.HeaderAdmin')
@stop

@section('content')
<div id="login_form">
{{Form::open(
	array(
		'url'=>'login'
	)
)}}
	<header>
		<img src="{{URL::to('assets/img/home_new.png')}}" class="logo">
		<div class="kost_informasi">
		Kost Informasi
		<div class="slogan">Butuh kost? Kami punya informasinya...</div>
	</header>
	<hr class="line">
	{{Form::text('username', Input::Old('username'), array('class'=>'inputStyle', 'placeholder'=>'username'))}}
	{{Form::password('password', array('class'=>'inputStyle', 'placeholder'=>'password'))}}
	{{Form::submit('Login', array('class'=>'loginButton'))}}
{{Form::close()}}
</div>
@stop
