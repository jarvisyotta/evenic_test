@extends('layout.MainLayout')

@section('header')
<header class="mainHead">
	<div id="content" class="mainHead">
		<div id="top">		
		<img src="{{URL::to('/assets/img/home_new.png');}}" width="120px" class="float_left">
		<div class="webTitle float_left">
		Kost Informasi
		<div class="small_webTitle align_right">Butuh kost? Kami punya informasinya...</div>
		</div>

		<div class="clear_both margin_btm"></div>

		<div class="cycle-slideshow slideBox">
			<img src="{{URL::to('assets/img/slide_1.jpg')}}" width="100%">
			<img src="{{URL::to('assets/img/slide_2.jpg')}}" width="100%">
			<img src="{{URL::to('assets/img/slide_3.jpg')}}" width="100%">
			<img src="{{URL::to('assets/img/slide_4.jpg')}}" width="100%">
		</div>
		</div>
	</div>
</header>
@stop

@section('content')
<div id="infoList">
	<div id="threeBox">
		<div class="infoTitle margin_btm_B">Informasi Seputar Kost</div>
		<img src="{{URL::to('assets/img/info_new.png')}}" width="40%">
	</div>
	<a href="{{URL::to('/lokasi')}}">
	<div id="threeBox">
		<div class="infoTitle margin_btm_B">Lokasi Kost</div>
		<img src="{{URL::to('assets/img/location_new.png')}}" width="40%">
	</div>
	</a>
	<div id="threeBox">
		<div class="infoTitle margin_btm_B">Hubungi Kost</div>
		<img src="{{URL::to('assets/img/phone_new.png')}}" width="40%">
	</div>
</div>
<div class="clear_both margin_btm"></div>
@stop

@section('footer')
@include('layout.Footer')
@stop