@extends('layout.AdminLayout')

@section('assets')
@parent
<link rel="stylesheet" href="{{URL::to('assets/css/adm_menu_style.css')}}">
<link rel="stylesheet" href="{{URL::to('assets/css/font-awesome.min.css')}}">
@stop

@section('header')
@include('layout.HeaderAdmin')
@stop

@section('content')
<div id="content">
	<div id="list_menu">
	<ul class="menu" id="menu">
		<li id="tmbh_data" onclick="show_submenu(this)" stat="close" class="main_menu">
			<i class="fa fa-file-text fa-lg"></i>
			Tambah Data Kost
		</li>
		<li class="submenu" id="submenu_tmbh_data">
			<ul>
				<li onclick="show_menu_form('tambah_data')"><i class="fa fa-file-o"></i>Tambah Kost</li>
				<li onclick="show_menu_form('upload_data')"><i class="fa fa-upload"></i>Upload Form Kost</li>
			</ul>
		</li>

		<li class="main_menu" onclick="show_menu_form('edit_data')">
			<i class="fa fa-edit fa-lg"></i>
			Edit Data
		</li>
		<a href="{{URL::to('/logout')}}" class="logout">
		<li class="main_menu">
			<i class="fa fa-sign-out fa-lg"></i>
			Log out
		</li>
		</a>
	</ul>
	</div>

	<div id="menu_field">
		
	</div>
</div>

<script src="{{URL::to('assets/js/menu.js')}}"></script>
@stop