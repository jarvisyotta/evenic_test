<?php

class MainController extends BaseController {
	protected $model;

	public function __construct(Main $model){
		$this->model = $model;
	}

	public function index(){
		return View::make('HalamanUtama');
	}

	public function lokasi(){
		$dataKost = $this->model->getAllDataTitik();
		$jData = count($dataKost);
		$dataKost = json_encode($dataKost);
		return View::make('LokasiKost', array("dataTitik"=>$dataKost, "jData"=>$jData));
	}

	public function getInfoTitik(){
		$id_kost = Input::get('id_kost');
		$InfoTitik = $this->model->getInfoTitik($id_kost);
		echo json_encode($InfoTitik);
	}
}
