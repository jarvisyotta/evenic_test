<?php

class AdminController extends BaseController {
	protected $model;

	public function __construct(Admin $model){
		$this->model = $model;
	}

	public function index(){
		return View::make('Admin');
	}

	public function login(){
		return View::make('AdminLoginPage');
	}

	public function login_user(){
		$input = Input::all();
		$username = $input['username'];
		$password = $input['password'];

		$valid = $this->model->checkUser($username, $password);

		if(!$valid){
			return Redirect::to('/login');
		} else {
			$login_key = md5(date('d-m-Y|h:i:s').$username);
			Session::push('login_key', $login_key);
			$this->model->insertSession($username, $login_key);

			return Redirect::to('/adm');
		}
	}

	public function logout_user(){
		$key = Session::get('login_key');
		if (isset($key[0])){
			$username = $this->model->logoutUser($key[0]);
		}
		return Redirect::to('/');
	}

	public function getForm(){
		
	}
}
