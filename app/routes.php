<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'MainController@index');
Route::get('/lokasi', 'MainController@lokasi');
Route::post('/getInfoTitik', 'MainController@getInfoTitik');

Route::get('/adm', array('before'=>'admin_access', 'uses'=>'AdminController@index'));
Route::get('/login', array('before'=>'cek_login', 'uses'=>'AdminController@login'));
Route::post('/login', 'AdminController@login_user');
Route::get('/logout', 'AdminController@logout_user');

Route::post('/getForm', 'AdminController@getForm');
