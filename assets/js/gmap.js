function initialize(){
	var pointArray = [];

	var mapOption = {
		center: new google.maps.LatLng(-0.018303, 109.336781),
		zoom: 13
	};

	//create google map canvas
	var map = new google.maps.Map(document.getElementById("mapKost"), mapOption);

	//show point
	for(var i = 0; i < jData; i++){
		var json_point = point_data[i];
		var point_kost = new google.maps.LatLng(json_point.lat, json_point.lng);

		pointArray.push(point_kost);

		var image;
		switch(json_point.jenis_kost){
			case 'Putra': 
					image = base_url + 'assets/img/kost_putra.png';
					break;
			case 'Putri':
					image = base_url + 'assets/img/kost_putri.png';
					break;
			case 'Campur':
					image = base_url + 'assets/img/kost_campur.png';
					break;
		}

		var markerOption = {
			position : point_kost,
			title : json_point.nama_kost,
			map : map,
			icon : image,
			point_id : json_point.id_kost
		};

		marker = new google.maps.Marker(markerOption);

		google.maps.event.addListener(marker, 'click', function(){
			infoTitik(this.point_id);
		});
	}

	// console.log(pointArray);

	// var arrayPoint = new google.maps.MVCArray(pointArray);

	// console.log(arrayPoint);
	
	// var heatmap = new google.maps.visualization.HeatmapLayer({
 	// 		data: arrayPoint
	// });

	// heatmap.setMap(map);

	// console.log("initialize");
	// window.location.hash = '#mapKost';
}

google.maps.event.addDomListener(window, 'load', initialize);

function infoTitik(point_id){
	$.post(base_url+'getInfoTitik', {"id_kost":point_id}, function(data){
		console.log(data.kost);
		tampilInfoBox(data.kost[0]);
	},"JSON");
}