function show_submenu(el){
	var menu = el.id;
	var stat = $(el).attr('stat');

	if (stat == 'close'){
		$('#submenu_' + menu).show('fast');
		$(el).attr('stat', 'open');
	} else {
		$('#submenu_' + menu).hide('fast');
		$(el).attr('stat', 'close');
	}
}

function show_menu_form(menu){
	$.ajax({
		type: "POST",
		url: "getForm",
		data: {menu: menu}
	}).done(function(data){
		console.log(data);
	});
}