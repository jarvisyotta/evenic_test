function tampilInfoBox(infokost){
   var docHeight = $(document).height();

   // console.log(content);
   var dataKost = "<div class='boxData'><div class='infoSep'><i class='fa fa-location-arrow fa-lg'></i>"+ infokost.alamat +"</div><div class='infoSep'><i class='fa fa-child fa-lg'></i>"+infokost.jenis_kost+"</div><div class='infoSep'><i class='fa fa-phone fa-lg'></i></div></div>";

   infoData = '<div id="dataKost"><h1 class="jdl">'+infokost.nama_kost+'</h1><hr class="garis">'+dataKost+'<div class="fotoKost"><img src="assets/img/no_image.jpg"></div></div>';
   infoData = infoData;
   content = '<div id="overlay">'+infoData+'</div>';

   // $("body").append(content);
   console.log(infoData);
   $("body").append(content);

   $('#dataKost')
      .css({
         'z-index': 1,
         'position': 'fixed',
         'background-color': '#C91E24',
         'opacity' : 1,
         'color': '#fff',
         'margin-left': '10%',
         'width': '80%',
         'height': '500px',
         'margin-top': '40px'
      });

   $("#overlay")
      .height(docHeight)
      .css({
         'z-index':0,
         'position': 'absolute',
         'top': 0,
         'left': 0,
         'background-color': 'rgba(0, 0, 0, 0.3)',
         'width': '100%',
         'z-index': 5000
      });

   $('.jdl')
      .css({
         'margin':'0',
         'padding':'0'
      });

   $('.garis')
      .css({
         'color':'#fff',
         'margin': '0',
         'padding': '0'
      });

   // console.log($('#overlay').html());
   $('#overlay').click(function(){
      $('#overlay').remove();
   });
} 
