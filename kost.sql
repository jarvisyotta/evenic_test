-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 19, 2014 at 05:24 AM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kost`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_kamar_kost`
--

CREATE TABLE IF NOT EXISTS `data_kamar_kost` (
  `id_kamar` int(11) NOT NULL AUTO_INCREMENT,
  `id_kost` int(11) NOT NULL,
  `nama_kamar` varchar(30) NOT NULL,
  `harga_kamar_bulan` varchar(10) NOT NULL,
  `harga_kamar_tahun` varchar(10) NOT NULL,
  `jumlah_kamar` varchar(5) NOT NULL,
  `fasilitas_kamar` text NOT NULL,
  `kamar_mandi` varchar(5) NOT NULL,
  `lebar_kamar` varchar(3) NOT NULL,
  `panjang_kamar` varchar(3) NOT NULL,
  PRIMARY KEY (`id_kamar`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `data_kamar_kost`
--

INSERT INTO `data_kamar_kost` (`id_kamar`, `id_kost`, `nama_kamar`, `harga_kamar_bulan`, `harga_kamar_tahun`, `jumlah_kamar`, `fasilitas_kamar`, `kamar_mandi`, `lebar_kamar`, `panjang_kamar`) VALUES
(1, 13, 'Kamar Tipe A', '', '', '', 'Air, Listrik', 'luar', '3', '6'),
(2, 14, 'Kamar Tipe A', '400000', '3800000', '10', 'Tempat Tidur, Lemari, Kipas angin, Meja belajar', 'luar', '3', '3'),
(3, 16, 'Kamar Tipe A', '275000', '3800000', '8', 'Kasur, Lemari, TV Bersama', 'luar', '2,6', '2,7'),
(4, 39, 'Kamar Tipe A', '', '3000000', '6', 'Bantal, Kasur', 'luar', '3', '2,5'),
(5, 39, 'Kamar Tipe A', '', '4500000', '42', 'Bantal, Kasur, Lemari', 'luar', '4', '2,5'),
(6, 40, 'Kamar Tipe A', '', '2300000', '12', 'Tidak ada', 'luar', '3', '3'),
(7, 53, 'Kamar Tipe A', '350000', '', '24', 'Lemari, Meja belajar, Kasur lantai, Bantal', 'luar', '3', '4'),
(8, 55, 'Kamar Tipe A', '900000', '', '22', 'Meja, Kursi, Lemari, Kasur', 'luar', '', ''),
(9, 55, 'Kamar Tipe A', '1500000', '', '23', 'Meja, Kursi, Lemari, Kasur, AC, TV, Kulkas', 'luar', '', ''),
(10, 56, 'Kamar Tipe A', '400000', '3200000', '30', 'Karpet', 'luar', '', ''),
(11, 57, 'Kamar Tipe A', '350000', '3000000', '21', '', 'luar', '', ''),
(12, 59, 'Kamar Tipe A', '250000', '3000000', '34', '', 'luar', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `data_kost`
--

CREATE TABLE IF NOT EXISTS `data_kost` (
  `id_kost` int(11) NOT NULL AUTO_INCREMENT,
  `lat` varchar(15) NOT NULL,
  `lng` varchar(15) NOT NULL,
  `nama_kost` varchar(40) NOT NULL,
  `jenis_kost` varchar(6) NOT NULL,
  `alamat` text NOT NULL,
  `nama_pemilik` varchar(100) NOT NULL,
  `total_kamar` varchar(4) NOT NULL,
  PRIMARY KEY (`id_kost`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=65 ;

--
-- Dumping data for table `data_kost`
--

INSERT INTO `data_kost` (`id_kost`, `lat`, `lng`, `nama_kost`, `jenis_kost`, `alamat`, `nama_pemilik`, `total_kamar`) VALUES
(1, '-00.0460361', '109.3090694', 'Kost Athiyyah', 'Campur', 'Gg. Damai No.1 Pontianak', '', ''),
(2, '-00.0580778', '109.3060611', 'Kost Putra Sukarela', 'Putra', 'Jl. Ampera Gg. Sukarela', '', ''),
(3, '-00.0488778', '109.3067', 'Kost Bersatu', 'Putri', 'Jl. Dansen Gg. Bersatu', '', ''),
(4, '-00.0403583', '109.2892361', 'Kost 7a', 'Putra', 'Jl. Ampera No. 7A', '', ''),
(5, '-00.0421722', '109.332875', 'Kost-28', 'Putri', 'Jl. Safe''i No. 28', '', ''),
(6, '-00.0565167', '109.3103222', 'Kost Angghi', 'Putra', 'Jl. M. Yamin Gg. Usaha Baru 2', '', ''),
(7, '-00.0603750', '109.3142167', 'Kost Diva', 'Campur', 'Jl. Kesehatan Gg. Sumber Agung 3 No.1', '', ''),
(8, '-00.0406417', '109.2894889', 'Kost Ega 1', 'Putri', 'Jl. Ampera', '', ''),
(9, '-00.0461389', '109.2945806', 'Kost Ega 2', 'Putri', 'Jl. Ampera No.20', '', ''),
(10, '-00.0385861', '109.3147028', 'Kost Gg Kusuma 4a', 'Campur', 'Gg. Kusuma No.4a', '', ''),
(11, '-00.0456667', '109.3174833', 'Kost Handayani', 'Campur', 'Jl. Dr. Sutomo No. 13A', '', ''),
(12, '-00.0325306', '109.318075', 'Kost Kencana', 'Campur', 'Jl. Ali Anyang Gg. Kencana II No.14', '', ''),
(13, '-00.0628556', '109.3074639', 'Kost Kurnia', 'Campur', 'Jl. Kurnia', '', ''),
(14, '-00.0413056', '109.29101390', 'Kost Kartika Sari', 'Putri', 'Jl. Ampera Gg. Wijaya No.2', 'Rita Kartika Sari', ''),
(15, '-00.0574139', '109.30878330', 'Kost Lestari', 'Putri', '', '', ''),
(16, '-0.05863', '109.310408', 'Kost M. Yunus', 'Putri', 'Jalan Nirbaya No.6 Kobar', 'Ida', ''),
(17, '-0.055469', '109.312872', 'Kost Aa'' Naufal', 'Putri', 'Gg. Bina Karya No.9A', '', ''),
(18, '-0.051473', '109.336198', 'Kost Putri Nurmala', 'Putri', 'Jl. Purnama Gg. Purnama 1 No.15', '', ''),
(19, '-00.0483167', '109.29572220', 'Kost Nuansa Bening', 'Putri', 'Jl. Ampera Gg. Nuansa Bening No.2', '', ''),
(20, '-00.0392806', '109.31491110', 'Kost Panama', 'Campur', 'Jl. P. Natakusuma Gg. Sumurbor No.5', '', ''),
(21, '', '', 'Kost Putra Kobar', '', '', '', ''),
(22, '-00.05754170', '109.30361670', 'Kost Putri Ampera', 'Putri', 'Jl. Ampera Komp. Bali Asri 1', '', ''),
(23, '-0.055457', '109.316713', 'Kost Putri Kel.Bersama', 'Putri', 'Gg. Keluarga Bersama Kobar', '', ''),
(24, '-00.04572220', '109.30917780', 'Kost Putri Sen. Indah', 'Putri', 'Gg. Sentarum Indah No.01', '', ''),
(25, '-00.04657220', '109.31789170', 'Kost Raya Montain', 'Campur', 'Jl. Dr. Sutomo No.03', '', ''),
(26, '-0.05671', '109.312115', 'Kost Rizky', 'Putra', 'Gg. Sinar Bersama Kobar', '', ''),
(27, '-00.04285830', '109.33156670', 'Kost Rm Intan', 'Campur', '', '', ''),
(28, '-00.04717500', '109.29543610', 'Kost Ummy', 'Putri', 'Jl. Ampera', '', ''),
(29, '-0.056782', '109.312248', 'Kost Vika', 'Putri', 'Gg. Sinar Bersama 5A Kobar', '', ''),
(30, '-00.02692500', '109.32738610', 'Kost Wanita', 'Putri', '', '', ''),
(31, '-00.04744720', '109.29568330', 'Kost Winda', 'Putra', 'Jl. Ampera', '', ''),
(32, '-00.05928330', '109.30772500', 'Kost Mitra', 'Campur', 'Jl. Prof. M. Yamin No.43', '', ''),
(33, '-00.04026390', '109.28915000', 'Kost Nisef Qua', 'Putri', 'Jl. Ampera', '', ''),
(34, '-00.02078330', '109.31879170', 'Kost Sahabat', 'Campur', 'Jl. H. Rais A Rahman No.171 B', '', ''),
(35, '-00.04396670', '109.29328890', 'Kost SPN 1B', 'Campur', 'Jl. Ampera Gg. SPN No.1B', '', ''),
(36, '-00.03913610', '109.28701940', 'Kost Umi & Uci', 'Putri', 'Jl. Ampera Gg. Abdul Karim No.4', '', ''),
(37, '-0.0612840', '109.348819', 'Kost Putri Anyza', 'Putri', 'Jl. A. Yani Sepakat 2 Blok Q', '', ''),
(38, '-0.058896', '109.351024', 'Kost Putra-Putri Blok C', 'Campur', 'Jl. A. Yani Sepakat 2 Blok C', '', ''),
(39, '-0.058714', '109.350593', 'Kost Umi', 'Campur', 'Jl. A. Yani Sepakat 2 Blok C', 'Umi', ''),
(40, '-0.058901', '109.35084', 'Kost GM5', 'Putri', 'Jl. A. Yani Sepakat 2 Blok C', 'Irwansyah', ''),
(41, '-0.058242', '109.350883', 'Kost GM4', 'Campur', 'Jl. A. Yani Sepakat 2 Blok C', '', ''),
(42, '-0.058815', '109.351169', 'Kost Al-Muslim 2', 'Putra', 'Jl. A. Yani Sepakat 2 Blok C', '', ''),
(43, '-0.061458', '109.348964', 'Kost Putra Blok Q', 'Putra', 'Jl. A. Yani Sepakat 2 Blok Q', '', ''),
(44, '-0.061473', '109.348976', 'Kost Siti Hajar', 'Putri', 'Jl. A. Yani Sepakat 2 Blok Q', '', ''),
(45, '-0.060522', '109.34902', 'Kost Alma', 'Putri', 'Jl. A. Yani Sepakat 2 Blok M', '', ''),
(46, '-0.060463', '109.349121', 'Kost Putri Bunda', 'Putri', 'Jl. A. Yani Sepakat 2 Blok M No.158', '', ''),
(47, '-0.060863', '109.349245', 'Kost No.154', 'Putri', 'Jl. A. Yani Sepakat 2 Blok M', '', ''),
(48, '-0.061053', '109.3493', 'Kost No.153', 'Putri', 'Jl. A. Yani Sepakat 2 Blok M', '', ''),
(49, '-0.061043', '109.349326', 'Kost No.152', 'Putri', 'Jl. A. Yani Sepakat 2 Blok M', '', ''),
(50, '-0.061182', '109.34955', 'Kost No.151', 'Putri', 'Jl. A. Yani Sepakat 2 Blok M', '', ''),
(51, '-0.060517', '109.349901', 'Kost Al-Muslim', 'Putra', 'Jl. A. Yani Sepakat 2 Blok H No.80', '', ''),
(52, '-0.059486', '109.350091', 'Kost Hijau', 'Campur', 'Jl. A. Yani Sepakat 2 Blok H No. 121', '', ''),
(53, '-0.056', '109.344', 'Kost Jambu', 'Campur', 'Jl. Perdana Gg. H. Samad No.11', 'Nur Aini', ''),
(54, '-00.04554170', '109.29367780', 'Kost Indri', 'Putra', 'Jl. Ampera Gg. Sumpolo No.23', '', ''),
(55, '-00.05948330', ' 109.35143330', 'Kost Dempo', 'Campur', 'Jl. A. Yani Sepakat 2', 'Drs. HM Daud Montain', ''),
(56, '-00.06596670', '109.34846670', 'Kost AM. Fajar', 'Putra', 'Sepakat 2 Jl. Anggrek', 'Sariman', ''),
(57, '-00.06145000', '109.35005000', 'Kost Evi', 'Putri', 'Sepakat 2 Blok N', 'Evi', ''),
(58, '-00.06513330', '109.34813330', 'Kost Blok T', 'Putra', 'Sepakat 2 Blok T', '', ''),
(59, '-00.06541670', '109.34730000', 'Kost Putri Kirani', 'Putri', 'Jl. Sepakat 2 Gg. Mawar No 4', 'Gusti Efendi, SE', ''),
(60, '-00.06428330', '109.34926670', 'Kost Alek', 'Putra', 'Sepakat 2', '', ''),
(61, '-00.06436670', '109.34938330', 'Kost Bg Firman', 'Putra', 'Sepakat 2', '', ''),
(62, '-00.06460000', '109.34953330', 'Kost Febris', 'Putri', 'Sepakat 2', '', ''),
(63, '-00.06473330', '109.34913330', 'Kost Nadine', 'Putri', 'Sepakat 2', '', ''),
(64, '-00.05113330', '109.30651940', 'Kost Arsiva', 'Putri', 'Jl. Pak Benceng', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `foto_kost`
--

CREATE TABLE IF NOT EXISTS `foto_kost` (
  `id_foto` int(11) NOT NULL AUTO_INCREMENT,
  `id_kost` int(11) NOT NULL,
  `nama_file` varchar(36) NOT NULL,
  PRIMARY KEY (`id_foto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jam_malam_kost`
--

CREATE TABLE IF NOT EXISTS `jam_malam_kost` (
  `id_waktu` int(11) NOT NULL AUTO_INCREMENT,
  `id_kost` int(11) NOT NULL,
  `hari` varchar(6) NOT NULL,
  `waktu` varchar(5) NOT NULL,
  PRIMARY KEY (`id_waktu`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(32) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
